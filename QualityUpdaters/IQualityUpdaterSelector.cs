﻿namespace csharp.QualityUpdaters
{
	public interface IQualityUpdaterSelector
	{
		IQualityUpdater GetQualityUpdater(Item item);
	}
}
