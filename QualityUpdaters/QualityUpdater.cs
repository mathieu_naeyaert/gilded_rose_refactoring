﻿namespace csharp.QualityUpdaters
{
	public class QualityUpdater : IQualityUpdater
    {
		public void UpdateItem(Item item_)
		{
			UpdateSellIn(item_);

			UpdateQuality(item_);
		}

		protected virtual void UpdateSellIn(Item item)
		{
			item.SellIn -= 1;
		}

		protected virtual void UpdateQuality(Item item)
		{
			if (item.Quality == 0)
				return;

			item.Quality -= (item.SellIn < 0 ? 2 : 1);

			if (item.Quality < 0)
				item.Quality = 0;
		}
	}
}
