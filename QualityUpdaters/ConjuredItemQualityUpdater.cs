﻿namespace csharp.QualityUpdaters
{
	public class ConjuredItemQualityUpdater : QualityUpdater
	{
		protected override void UpdateQuality(Item item)
		{
			if (item.Quality == 0)
				return;

			item.Quality -= (item.SellIn < 0 ? 4 : 2);

			if (item.Quality < 0)
				item.Quality = 0;
		}
	}
}
