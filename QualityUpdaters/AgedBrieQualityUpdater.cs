﻿namespace csharp.QualityUpdaters
{
	public class AgedBrieQualityUpdater : QualityUpdater
	{
		protected override void UpdateQuality(Item item)
		{
			if (item.Quality == 50)
				return;

			item.Quality += 1;
		}
	}
}
