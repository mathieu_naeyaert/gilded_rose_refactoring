﻿namespace csharp.QualityUpdaters
{
	public interface IQualityUpdater
	{
		void UpdateItem(Item item_);
	}
}
