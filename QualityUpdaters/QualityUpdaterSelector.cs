﻿namespace csharp.QualityUpdaters
{
	public class QualityUpdaterSelector : IQualityUpdaterSelector
	{
		public IQualityUpdater GetQualityUpdater(Item item)
		{
			var name = item.Name.ToLower();

			if (name == "aged brie")
				return new AgedBrieQualityUpdater();

			if (name.StartsWith("backstage passes"))
				return new BackStageQualityUpdater();

			if (name.StartsWith("conjured"))
				return new ConjuredItemQualityUpdater();

			if (name.StartsWith("sulfuras"))
				return new SulfurasQualityUpdater();

			return new QualityUpdater();
		}
	}
}
