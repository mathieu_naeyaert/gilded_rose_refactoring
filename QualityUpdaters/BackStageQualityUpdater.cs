﻿namespace csharp.QualityUpdaters
{
	public class BackStageQualityUpdater : QualityUpdater
	{
		protected override void UpdateQuality(Item item)
		{
			if (item.Quality == 0)
				return;

			if(item.SellIn < 0)
			{
				item.Quality = 0;
				return;
			}

			if (item.SellIn > 10)
			{
				item.Quality += 1;
			}
			else if (item.SellIn > 5)
			{
				item.Quality += 2;
			}
			else
			{
				item.Quality += 3;
			}

			if (item.Quality > 50)
				item.Quality = 50;
		}
	}
}
