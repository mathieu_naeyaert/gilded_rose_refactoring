﻿using csharp.QualityUpdaters;
using System.Collections.Generic;

namespace csharp
{
	public class GildedRose
	{
		IList<UpdatableItem> Items;

		readonly IQualityUpdaterSelector QualityUpdaterSelector;
		
		public GildedRose(IList<Item> Items, IQualityUpdaterSelector updaterSelector = null)
		{
			this.Items = new List<UpdatableItem>();
			QualityUpdaterSelector = updaterSelector ?? new QualityUpdaterSelector();

			foreach (var item in Items)
			{
				this.Items.Add(new UpdatableItem(item, QualityUpdaterSelector.GetQualityUpdater(item)));
			}
		}

		public void UpdateQuality()
		{
			foreach(var item in Items)
			{
				item.UpdateQuality();
			}
		}

		private class UpdatableItem
		{
			public Item Item;

			public IQualityUpdater QualityUpdater;

			public UpdatableItem(Item item, IQualityUpdater qualityUpdater)
			{
				Item = item;
				QualityUpdater = qualityUpdater;
			}

			public void UpdateQuality()
			{
				QualityUpdater.UpdateItem(Item);
			}
		}
	}
}
