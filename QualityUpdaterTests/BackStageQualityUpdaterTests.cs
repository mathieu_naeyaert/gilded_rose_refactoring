﻿using csharp.QualityUpdaters;
using NUnit.Framework;

namespace csharp.QualityUpdaterTests
{
	[TestFixture]
	public class BackStageQualityUpdaterTests
	{
		[Test]
		public void TestSellInUpdate()
		{
			var updater = new BackStageQualityUpdater();
			var item = new Item() { SellIn = 5, Quality = 10 };

			updater.UpdateItem(item);

			Assert.AreEqual(item.SellIn, 4);
		}

		[Test]
		[TestCase(15, 10, 11, TestName = "Test quality more than ten days before concert")]
		[TestCase(10, 10, 12, TestName = "Test quality ten days before concert")]
		[TestCase(5, 10, 13, TestName = "Test quality five days before concert")]
		[TestCase(0, 10, 0, TestName = "Test quality after concert")]
		[TestCase(1, 49, 50, TestName = "Test quality never greater than 50")]
		public void TestQualityUpdate(int sellIn, int initialQuality, int finalQuality)
		{
			var updater = new BackStageQualityUpdater();
			var item = new Item() { SellIn = sellIn, Quality = initialQuality };

			updater.UpdateItem(item);

			Assert.AreEqual(item.Quality, finalQuality);
		}
	}
}
