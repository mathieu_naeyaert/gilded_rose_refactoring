﻿using csharp.QualityUpdaters;
using NUnit.Framework;

namespace csharp.QualityUpdaterTests
{
	[TestFixture]
	public class SulfurasQualityUpdaterTests
	{
		[Test]
		public void TestSellInUpdate()
		{
			var updater = new SulfurasQualityUpdater();
			var item = new Item() { SellIn = 5, Quality = 10 };

			updater.UpdateItem(item);

			Assert.AreEqual(item.SellIn, 5);
		}

		[Test]
		public void TestQualityUpdate()
		{
			var updater = new SulfurasQualityUpdater();
			var item = new Item() { SellIn = 5, Quality = 10 };

			updater.UpdateItem(item);

			Assert.AreEqual(item.Quality, 10);
		}
	}
}
