﻿using csharp.QualityUpdaters;
using NUnit.Framework;

namespace csharp.QualityUpdaterTests
{
	[TestFixture]
	public class QualityUpdaterTests
	{
		[Test]
		public void TestSellInUpdate()
		{
			var updater = new QualityUpdater();
			var item = new Item() { SellIn = 5, Quality = 10};

			updater.UpdateItem(item);

			Assert.AreEqual(item.SellIn, 4);
		}

		[Test]
		public void TestQualityUpdate()
		{
			var updater = new QualityUpdater();
			var item = new Item() { SellIn = 5, Quality = 10 };

			updater.UpdateItem(item);

			Assert.AreEqual(item.Quality, 9);
		}

		[Test]
		public void TestQualityUpdateAfterSellByDate()
		{
			var updater = new QualityUpdater();
			var item = new Item() { SellIn = 0, Quality = 10 };

			updater.UpdateItem(item);

			Assert.AreEqual(item.Quality, 8);
		}

		[Test]
		public void TestQualityUpdateNeverNegative()
		{
			var updater = new QualityUpdater();
			var item = new Item() { SellIn = 5, Quality = 0 };

			updater.UpdateItem(item);

			Assert.AreEqual(item.Quality, 0);
		}

		[Test]
		public void TestQualityUpdateNeverNegativeAfterSellByDate()
		{
			var updater = new QualityUpdater();
			var item = new Item() { SellIn = 0, Quality = 1 };

			updater.UpdateItem(item);

			Assert.AreEqual(item.Quality, 0);
		}
	}
}
