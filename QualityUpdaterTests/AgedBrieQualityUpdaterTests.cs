﻿using csharp.QualityUpdaters;
using NUnit.Framework;

namespace csharp.QualityUpdaterTests
{
	[TestFixture]
	public class AgedBrieQualityUpdaterTests
	{
		[Test]
		public void TestSellInUpdate()
		{
			var updater = new AgedBrieQualityUpdater();
			var item = new Item() { SellIn = 5, Quality = 10 };

			updater.UpdateItem(item);

			Assert.AreEqual(item.SellIn, 4);
		}

		[Test]
		public void TestQualityUpdate()
		{
			var updater = new AgedBrieQualityUpdater();
			var item = new Item() { SellIn = 5, Quality = 10 };

			updater.UpdateItem(item);

			Assert.AreEqual(item.Quality, 11);
		}

		[Test]
		public void TestMaxQualityUpdate()
		{
			var updater = new AgedBrieQualityUpdater();
			var item = new Item() { SellIn = 5, Quality = 50 };

			updater.UpdateItem(item);

			Assert.AreEqual(item.Quality, 50);
		}
	}
}
