﻿using csharp.QualityUpdaters;
using NUnit.Framework;
using System;

namespace csharp.QualityUpdaterTests
{
	[TestFixture]
	public class QualityUpdaterSelectorTests
	{
		[Test]
		[TestCase("Foo", typeof(QualityUpdater))]
		[TestCase("Aged Brie", typeof(AgedBrieQualityUpdater))]
		[TestCase("Backstage passes for concert", typeof(BackStageQualityUpdater))]		
		[TestCase("Conjured item", typeof(ConjuredItemQualityUpdater))]
		[TestCase("Sulfuras", typeof(SulfurasQualityUpdater))]
		public void TestSelector(string itemName, Type qualityUpdaterType)
		{
			var item = new Item() { Name = itemName };
			var selector = new QualityUpdaterSelector();

			var updater = selector.GetQualityUpdater(item);

			Assert.AreEqual(updater.GetType(), qualityUpdaterType);
		}
	}
}
