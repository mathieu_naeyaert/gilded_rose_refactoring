# How to launch the application

After pulling the repository, open the csharp.sln file with an instance of Microsoft Visual Studio.

The solution contains only one project : csharp. This project targets .Net Framework 4.7.2 and will be the StartUp project of the solution.

You can just start the application to launch the application in a console window.

# How to launch the tests

With the opend solution, you have to open the 'Test explorer' window by going in the 'Test' menu and selecting 'Test explorer'.

This window will automatically display all the unit tests in the solution.

You can then run all tests or select and run a specific test.