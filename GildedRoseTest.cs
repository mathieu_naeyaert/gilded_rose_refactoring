﻿using csharp.QualityUpdaters;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace csharp
{
	[TestFixture]
	public class GildedRoseTest
	{
		[Test]
		public void TestUpdaterCall()
		{
			var qualityUpdater = new Mock<IQualityUpdater>();
			qualityUpdater.Setup(foo => foo.UpdateItem(It.IsAny<Item>()));

			var updaterSelector = new Mock<IQualityUpdaterSelector>();
			updaterSelector.Setup(foo => foo.GetQualityUpdater(It.IsAny<Item>())).Returns(qualityUpdater.Object);

			IList<Item> Items = new List<Item> { new Item { Name = "", SellIn = 0, Quality = 0 } };
			GildedRose app = new GildedRose(Items, updaterSelector.Object);
			app.UpdateQuality();

			updaterSelector.Verify(foo => foo.GetQualityUpdater(It.IsAny<Item>()), Times.Once());
			qualityUpdater.Verify(foo => foo.UpdateItem(It.IsAny<Item>()), Times.Once());
		}
	}
}
